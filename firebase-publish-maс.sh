#!/bin/bash

# How to use:
# Publish app: ./firebase-publish-mac.sh
appFolder=app
echo "Publishing app..."
./gradlew incrementVersionCode --console=plain
./gradlew app:assembleRelease app:appDistributionUploadRelease