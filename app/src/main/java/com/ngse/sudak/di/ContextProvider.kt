package com.ngse.sudak.di

import android.content.Context

object ContextProvider : DependentProvider<Context>()
