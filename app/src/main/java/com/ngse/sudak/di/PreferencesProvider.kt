package com.ngse.sudak.di

import com.ngse.sudak.data.Preferences


object PreferencesProvider : DependentProvider<Preferences>()
