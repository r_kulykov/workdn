package com.ngse.sudak.di.apiclient

import com.ngse.sudak.BuildConfig.API_BASE_URL
import com.ngse.sudak.api.ApiClient
import com.ngse.sudak.api.ApiEndpoint
import io.reactivex.plugins.RxJavaPlugins
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import com.ngse.sudak.di.IndependentProvider
import com.ngse.sudak.di.ObjectMapperProvider

internal object ApiClientProvider : IndependentProvider<ApiClient>() {


    private val httpClient by HttpClientProvider()
    private val objectMapper by ObjectMapperProvider()

    init {
        RxJavaPlugins.setErrorHandler { it.printStackTrace() }
    }

    override fun initInstance(): ApiClient = Retrofit.Builder()
        .baseUrl(API_BASE_URL)
        .client(httpClient)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(objectMapper))
        .build()
        .create(ApiEndpoint::class.java)
        .run { ApiClient(this) }


}


