package com.ngse.sudak.di.apiclient

import android.annotation.SuppressLint
import com.google.gson.Gson
import com.ngse.sudak.BuildConfig
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONException
import com.ngse.sudak.di.IndependentProvider
import com.ngse.sudak.di.PreferencesProvider
import java.io.IOException
import java.util.concurrent.TimeUnit


internal object HttpClientProvider : IndependentProvider<OkHttpClient>() {

    private const val TIMEOUT = 60L

    private val localStorage by PreferencesProvider()


    override fun initInstance(): OkHttpClient = OkHttpClient.Builder()
        .addNetworkInterceptor(HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.NONE
            }
        })
        .readTimeout(TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
        .addInterceptor(authInterceptor())
        .build()

    private fun authInterceptor() = Interceptor { chain ->
        val newRequestBuilder = chain.request().newBuilder()
        /*if (localStorage.isLogin()) {
            newRequestBuilder.header("Authorization", "Bearer ${localStorage.getAccessToken()}")
        }*/
        chain.proceed(newRequestBuilder.build())
    }

}
