package com.ngse.sudak.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.util.*

object ObjectMapperProvider : IndependentProvider<Gson>() {


    override fun initInstance(): Gson = GsonBuilder()
        //.registerTypeAdapter(Calendar::class.java, CalendarTypeAdapter())
        //.registerTypeAdapter(GregorianCalendar::class.java, CalendarTypeAdapter())
        .create()

    /*private class CalendarTypeAdapter : TypeAdapter<Calendar>() {
        companion object {
            val PARSE_DATE_FORMATS = arrayOf(MAIN_SERVER_DATE_FORMAT)
        }


        @Throws(IOException::class)
        override fun write(writer: JsonWriter, value: Calendar?) {
            if (value == null) writer.nullValue()
            else {
                writer.value(value.serverDateFormat())
            }
        }

        @Throws(IOException::class)
        override fun read(jsonReader: JsonReader): Calendar? {
            if (jsonReader.peek() == JsonToken.NULL) {
                jsonReader.nextNull()
                return null
            }
            var calendar: Calendar? = null
            var longTime = jsonReader.nextLong()
            if (!longTime.equals(0)) {
                calendar = Calendar.getInstance()
                calendar.timeInMillis = longTime * 1000
            }

            return calendar
        }
    }*/

}
