package com.ngse.sudak.api


import com.ngse.sudak.data.BaseListResponse
import com.ngse.sudak.data.BaseObjectResponse
import io.reactivex.Single

typealias Response<T> = Single<T>
typealias ListResponse<T> = Single<BaseListResponse<T>>
typealias ObjectResponse<T> = Single<BaseObjectResponse<T>>


@JvmSuppressWildcards
interface ApiEndpoint {

}