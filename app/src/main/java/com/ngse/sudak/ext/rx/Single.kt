package com.ngse.sudak.ext.rx

import android.annotation.SuppressLint
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import com.ngse.sudak.ui.base.BasePresenter
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers.io as IO

typealias OnSuccess<R> = (response: R) -> Unit
typealias OnError = (throwable: Throwable) -> Unit


@SuppressLint("CheckResult")
fun <R, P : BasePresenter<*>> Single<R>.subscribe(
    withProgress: Boolean,
    presenter: P,
    onError: OnError,
    onSuccess: OnSuccess<R>
): Disposable {
    if (withProgress)
        presenter.view?.showProgressIndicator()

    return subscribeOn(IO())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSuccess { if (withProgress) presenter.view?.hideProgressIndicator() }
        .doOnError { if (withProgress) presenter.view?.hideProgressIndicator() }
        .subscribe(onSuccess, onError)
}


