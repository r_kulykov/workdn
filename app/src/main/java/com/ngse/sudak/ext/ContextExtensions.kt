package com.ngse.sudak.ext

import android.Manifest
import android.app.Dialog
import android.app.NotificationManager
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.TypedArray
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.ngse.sudak.R

inline fun Context.dialog(@LayoutRes layoutRes: Int, init: Dialog.() -> Unit = {}): Dialog {
    return Dialog(this, R.style.AppDialog).apply {
        setContentView(layoutRes)
        init(this)
    }
}

fun Context.isPermissionGranted(vararg permissions: String): Boolean {
    val notGranted = permissions.filter { ActivityCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED }
    return notGranted.isEmpty()
}

fun Context.isCameraPermissionGranted() = isPermissionGranted(Manifest.permission.CAMERA)

fun Context.getColorCompat(colorResId: Int): Int {
    return ContextCompat.getColor(this, colorResId)
}

fun Context.getDrawableCompat(drawableId: Int): Drawable {
    return ContextCompat.getDrawable(this, drawableId)!!
}

fun Context.getString(key: String): String {
    val id = resources.getIdentifier(key, "string", packageName)
    if (id != 0) {
        return getString(id)
    }

    return ""
}

fun Context.getString(key: String, vararg args: Any): String {
    val id = resources.getIdentifier(key, "string", packageName)
    if (id != 0) {
        return getString(id, *args)
    }

    return ""
}

fun Context.getLayoutInflater() = LayoutInflater.from(this)

fun Context.useStyledAttributes(attrs: AttributeSet, styleableRes: IntArray, action: TypedArray.() -> Unit) {
    obtainStyledAttributes(attrs, styleableRes).run {
        action()
        recycle()
    }
}

fun Context.getDisplayMetrics(): DisplayMetrics {
    val metrics = DisplayMetrics()
    windowManager.defaultDisplay.getMetrics(metrics)
    return metrics
}

fun Context.getScreenWidthPx() = getDisplayMetrics().widthPixels
fun Context.getScreenHeightPx() = getDisplayMetrics().heightPixels

val Context.notificationManager
    get() = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

val Context.windowManager
    get() = getSystemService(Context.WINDOW_SERVICE) as WindowManager