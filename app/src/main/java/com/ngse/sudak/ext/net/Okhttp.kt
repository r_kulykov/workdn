package com.ngse.sudak.ext.net

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.InputStream


fun String.getRequestBody(): RequestBody = RequestBody.create(MediaType.parse("text/plain"), this)
fun Double.getRequestBody(): RequestBody = RequestBody.create(MediaType.parse("text/plain"), this.toString())
fun Int.getRequestBody(): RequestBody = RequestBody.create(MediaType.parse("text/plain"), this.toString())

fun InputStream.getMultipartBody(): MultipartBody.Part {
    val body = RequestBody.create(MediaType.parse("multipart/form-data"), readBytes())
    return MultipartBody.Part.createFormData("image", "image", body)
}

