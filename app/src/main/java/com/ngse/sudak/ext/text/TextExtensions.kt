package com.ngse.sudak.ext.text

import android.os.Build
import android.text.*
import android.text.style.ForegroundColorSpan


@Suppress("DEPRECATION")
fun CharSequence?.formatAsHtml() = this?.let {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) Html.fromHtml(it.toString())
    else Html.fromHtml(it.toString(), Html.FROM_HTML_MODE_LEGACY)
}

fun getColoredString(fulltext: String?, subtext: String, color: Int): Spannable? {
    if (fulltext == null)
        return null
    val spannable = SpannableString(fulltext)
    val i = fulltext.indexOf(subtext)
    if (i > 0)
        spannable.setSpan(ForegroundColorSpan(color), i, i + subtext.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    return spannable
}
