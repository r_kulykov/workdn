package com.ngse.sudak.ext.net

import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import com.ngse.sudak.di.ContextProvider


fun isOffline() = !isOnline()

fun isOnline(): Boolean {
    val context by ContextProvider()
    val manager = context.getSystemService(CONNECTIVITY_SERVICE) as? ConnectivityManager
    val networkInfo = manager?.activeNetworkInfo
    return networkInfo != null && networkInfo.isConnected
}



