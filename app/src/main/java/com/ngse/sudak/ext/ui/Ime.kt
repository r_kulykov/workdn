package com.ngse.sudak.ext.ui

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import com.ngse.sudak.ext.tryTo


fun Activity.hideKeyboard() {
    val view = window.currentFocus ?: return
    val manager = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
    if (manager != null) tryTo { manager.hideSoftInputFromWindow(view.windowToken, 0) }
}

fun Fragment.hideKeyboard() {
    val view = activity?.window?.currentFocus ?: return
    val manager = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
    if (manager != null) tryTo { manager.hideSoftInputFromWindow(view.windowToken, 0) }
}

fun View.showKeyboard() {
    requestFocus()
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(this, InputMethodManager.SHOW_FORCED)
}

fun View.hideKeyboard() {
    val manager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
    if (manager != null) tryTo { manager.hideSoftInputFromWindow(windowToken, 0) }
}

fun Activity.copyToClipboard(text: String?) {
    val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val clip = ClipData.newPlainText("", text)
    clipboard.setPrimaryClip(clip)
}
