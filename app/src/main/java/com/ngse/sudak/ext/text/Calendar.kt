package com.ngse.sudak.ext.text

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

const val MAIN_SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
const val DATE_SHORT_FORMAT = "dd.MM.yyyy"
const val DATE_FULL_FORMAT = "dd MMMM yyyy"
const val TIME_COMMENT = "HH:mm"

@SuppressLint("SimpleDateFormat")
fun Calendar.serverDateFormat(): String {
    return SimpleDateFormat(MAIN_SERVER_DATE_FORMAT).format(this.time)
}

@SuppressLint("SimpleDateFormat")
fun Calendar.dateFullFormat(): String {
    return SimpleDateFormat(DATE_FULL_FORMAT).format(this.time)
}
