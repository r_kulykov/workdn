package com.ngse.sudak.ext

import android.content.Context
import android.content.res.Resources
import android.util.TypedValue

val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()

val Float.dp: Float
    get() = (this / Resources.getSystem().displayMetrics.density)

val Float.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()


fun Context.dpToPixels(dp: Float) =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics)