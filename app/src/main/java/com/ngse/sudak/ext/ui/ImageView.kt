package com.ngse.sudak.ext.ui

import android.widget.ImageView
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.ngse.sudak.GlideApp
import com.ngse.sudak.R


fun ImageView.load(
    model: Any?,
    requestOptions: RequestOptions? = null,
    @DrawableRes placeholder: Int? = 0,
    @DrawableRes error: Int? = placeholder

) {
    GlideApp.with(this).load(model)
        .placeholder(placeholder ?: 0)
        .error(error ?: 0)
        .apply(requestOptions ?: RequestOptions())
        .into(this)
}

fun ImageView.loadAvatar(
    model: Any?,
    requestOptions: RequestOptions? = null,
    @DrawableRes placeholder: Int? = android.R.color.transparent,
    @DrawableRes error: Int? = placeholder

) = load(model, requestOptions, placeholder, error)

fun ImageView.loadWithRoundedCorners(
    model: Any?,
    @DimenRes radius: Int = R.dimen.round_corners_radius,
    @DrawableRes placeholder: Int = android.R.color.transparent,
    @DrawableRes error: Int = placeholder

) {
    val valueInPixels = context.resources.getDimension(radius)
    var requestOptions = RequestOptions()
    requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(valueInPixels.toInt()))
    load(model, requestOptions, placeholder, error)
}
