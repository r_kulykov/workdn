package com.ngse.sudak.ext.ui

import android.app.Dialog
import android.content.Context
import androidx.annotation.LayoutRes
import com.ngse.sudak.R
import com.ngse.sudak.ext.dialog
import com.ngse.sudak.ui.adapters.ListChooserAdapter
import kotlinx.android.synthetic.main.dialog_list_chooser.*


inline fun dialog(
    context: Context, @LayoutRes layoutRes: Int,
    init: Dialog.() -> Unit = {}
): Dialog {
    return context.dialog(layoutRes, init)
}

fun Context.initChooserDialog(
    items: List<String>, onRightButtonClick: (common: String) -> Unit
) = dialog(this, R.layout.dialog_list_chooser) {
    rvList.adapter = ListChooserAdapter(onItemClicked = {
        dismiss()
        onRightButtonClick(it)
    })
        .apply {
            setItems(items)
        }
    show()
}


