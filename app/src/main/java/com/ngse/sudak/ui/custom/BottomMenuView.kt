package com.ngse.sudak.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.ngse.sudak.R
import com.ngse.sudak.ext.getLayoutInflater
import com.ngse.sudak.ui.navigation.ActivityViewNavigator
import kotlinx.android.synthetic.main.item_bottom_menu.view.*

class BottomMenuView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : LinearLayout(context, attrs) {

    private val viewNavigator by lazy { ActivityViewNavigator(context as FragmentActivity) }

    var positionType = Position.UNKNOWN
        set(position) = position.let { field = it.apply(::updateMenu) }

    init {
        val arr = context.obtainStyledAttributes(attrs, R.styleable.BottomMenuView, 0, 0)
        val fooType =
            Position.values().first { it.id == arr.getInt(R.styleable.BottomMenuView_position, 0) }
        arr.recycle()
        positionType = fooType
        setBackgroundResource(R.drawable.bg_bottom_view)
        orientation = LinearLayout.HORIZONTAL
        gravity = Gravity.CENTER
        //background = ContextCompat.getDrawable(context, android.R.color.white)
        layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            resources.getDimensionPixelSize(R.dimen.bottom_bar_height)
        )
        initAll()
    }

    private fun initAll() {
        for (position in 0 until 4) {
            val (drawableTopResId, stringResId) = getResIds(position)
            addView(context.getLayoutInflater().inflate(R.layout.item_bottom_menu, null).apply {
                tag = Position.values().first { it.id == position }
                ivMenuIcon.run {
                    setImageResource(drawableTopResId)
                    isSelected = tag == positionType
                }
                tvMenuTitle.setText(stringResId)
                val outValue = TypedValue()
                context.theme.resolveAttribute(
                    android.R.attr.selectableItemBackgroundBorderless,
                    outValue,
                    true
                )
                //setBackgroundResource(outValue.resourceId)
                layoutParams = LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, 1.0f)
                setOnClickListener {
                    when (it.tag) {
                        Position.SEARCH -> viewNavigator.showVacanciesActivity()
                        Position.NEW -> viewNavigator.showNumbersActivity()
                        Position.FAVORITE -> viewNavigator.showMainActivity()
                        Position.PROFILE -> viewNavigator.showMapActivity()
                    }
                }
            })
        }
        updateMenu(positionType)
    }

    private fun getResIds(id: Int) = when (Position.values().first { it.id == id }) {
        Position.SEARCH -> R.drawable.ic_search_selector to R.string.map
        Position.NEW -> R.drawable.ic_news_selector to R.string.numbers
        Position.FAVORITE -> R.drawable.ic_favorite_selector to R.string.main
        Position.PROFILE -> R.drawable.ic_profile_selector to R.string.news
        Position.UNKNOWN -> 0 to 0
    }

    private fun updateMenu(position: Position) {
        for (i in 0 until childCount) {
            val parent = getChildAt(i) as LinearLayout
            for (j in 0 until parent.childCount) {
                (parent.getChildAt(j) as View).isSelected = position == parent.tag
            }
        }
    }


    enum class Position(val id: Int) {
        UNKNOWN(-1), SEARCH(0), NEW(1), FAVORITE(2), PROFILE(3);

        companion object : List<Position> by values().toList()
    }


}
