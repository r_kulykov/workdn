package com.ngse.sudak.ui.base

import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.PopupWindow
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


abstract class BaseMenuItemActivity : BaseActivity() {

    private var isFragmentAdded: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isFragmentAdded =
            savedInstanceState?.getBoolean(FRAGMENT_KEY) ?: false
        if (!isFragmentAdded) {
            showFirstFragment()
            isFragmentAdded = true
        }
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)

        EventBus.getDefault().register(this)
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(FRAGMENT_KEY, isFragmentAdded)
    }

    override fun onBackPressed() {
        if (isTaskRoot)
            finish()//use finish() for remove pinned few second pinned transaction image on main activity
        else
            super.onBackPressed()
    }


    open fun showFirstFragment() {}

}


