package com.ngse.sudak.ui.activities.login

import android.os.Bundle
import android.os.Handler
import com.ngse.sudak.R
import com.ngse.sudak.ui.base.BaseMvpActivity
import kotlinx.android.synthetic.main.activity_login.*
import moxy.ktx.moxyPresenter

class LoginActivity : BaseMvpActivity<LoginPresenter, LoginView>(), LoginView {

    override val layoutRes: Int = R.layout.activity_login
    override val presenter by moxyPresenter { LoginPresenter() }
    override val mvpView: LoginView = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginButton.setOnClickListener {
            showProgressIndicator()
            Handler().postDelayed({
                navigator.showSplashActivity()
            }, 2000L)
        }
        registrationButton.setOnClickListener {
            navigator.showRegistrationActivity()
        }
    }

}