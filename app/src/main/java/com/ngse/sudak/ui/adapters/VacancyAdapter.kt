package com.ngse.sudak.ui.adapters

import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.view.isVisible
import com.ngse.sudak.R
import com.ngse.sudak.data.Vacancy
import com.ngse.sudak.ui.base.BaseAdapter
import com.ngse.sudak.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.list_item_vacantion.view.*

class VacancyAdapter :
    BaseAdapter<Vacancy, VacancyAdapter.ViewHolder>() {

    var deleteVacancy: ((Vacancy) -> Unit)? = null
    var likeVacancy: ((Vacancy) -> Unit)? = null
    var onItemClick: ((Vacancy) -> Unit)? = null

    override fun newViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(R.layout.list_item_vacantion, parent)

    inner class ViewHolder(@LayoutRes layoutRes: Int, parent: ViewGroup) :
        BaseViewHolder<Vacancy>(layoutRes, parent) {

        private var isFavorite = false

        override fun updateView(item: Vacancy) = with(itemView) {
            item.run {
                tvStatus.isVisible = isNew
                tvTitle.text = title
                tvPrice.text = price
                tvCompanyName.text = companyName
                tvLocation.text = location
                tvLastUpdated.text = lastUpdated
                llToFavorite.setOnClickListener {
                    isFavorite = !isFavorite
                    if (isFavorite){
                        tvToFavorite.setText(R.string.from_favorite)
                    } else {
                        tvToFavorite.setText(R.string.to_favorite)
                    }
                }
                llNotInterest.setOnClickListener { notifyItemRemoved(adapterPosition) }
                setOnClickListener { onItemClick?.invoke(this) }
            }
        }
    }

}