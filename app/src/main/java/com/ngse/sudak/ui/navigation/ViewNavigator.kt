package com.ngse.sudak.ui.navigation

import android.app.Activity
import android.content.Intent
import kotlin.reflect.KClass

interface ViewNavigator {

    fun clearBackStack()
    fun startActivity(intent: Intent)
    fun startActivity(activityClass: KClass<out Activity>, clearBackStack: Boolean = false)

    fun showMainActivity()
    fun showMapActivity()
    fun showVacanciesActivity()
    fun showNumbersActivity()
    fun showVacancyDetailActivity()

    fun showWebBrowser(url: String)
    fun showSendEmail(email: String)
    fun showCallPhone(phone: String)
    fun showLoginActivity()
    fun showSplashActivity()
    fun showRegistrationActivity()
    fun showEditProfile()

}
