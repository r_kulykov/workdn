package com.ngse.sudak.ui.base

import com.ngse.sudak.R
import com.ngse.sudak.api.ListResponse
import com.ngse.sudak.api.ObjectResponse
import com.ngse.sudak.data.ApiError
import com.ngse.sudak.data.BaseData
import com.ngse.sudak.di.ContextProvider
import com.ngse.sudak.di.ObjectMapperProvider
import com.ngse.sudak.di.PreferencesProvider
import com.ngse.sudak.di.apiclient.ApiClientProvider
import com.ngse.sudak.ui.navigation.ViewNavigator
import com.ngse.sudak.ext.net.isOffline
import com.ngse.sudak.ext.rx.OnError
import com.ngse.sudak.ext.rx.OnSuccess
import com.ngse.sudak.ext.rx.subscribe
import com.ngse.sudak.ext.tryOrNull
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import moxy.MvpPresenter
import okhttp3.ResponseBody
import retrofit2.HttpException
import java.net.SocketTimeoutException

abstract class BasePresenterImpl<V : BaseView> : MvpPresenter<V>(), BasePresenter<V> {

    private val compositeDisposable = CompositeDisposable()

    protected val localStorage by PreferencesProvider()
    protected val apiClient by ApiClientProvider()
    protected val context by ContextProvider()
    protected val mapper by ObjectMapperProvider()


    private lateinit var navigator: ViewNavigator

    override fun onDestroy() {
        compositeDisposable.dispose()
    }

    override fun setNavigator(navigator: ViewNavigator) {
        this.navigator = navigator
    }

    override fun getNavigator(): ViewNavigator = navigator

    protected fun onError(throwable: Throwable) {
        throwable.printStackTrace()
        viewState?.hideProgressIndicator()
        viewState?.onError(throwable)
        onError()
    }

    protected open fun onError() = Unit

    protected fun setProgressIndicator(active: Boolean) {
        if (active) viewState?.showProgressIndicator()
        else viewState?.hideProgressIndicator()
    }

    private fun V.onError(baseData: BaseData) = showMessage(baseData.message)

    protected fun V.onError(throwable: Throwable) = when {
        throwable is SocketTimeoutException -> showMessage(R.string.timeout_error)
        throwable is HttpException -> showMessage(throwable.readMessage())
        isOffline() -> showMessage(R.string.network_error)
        else -> showMessage(throwable.message)
    }

    private fun HttpException.readMessage(): String? {
        return response()?.errorBody()?.readMessage() ?: message()
    }

    private fun ResponseBody.readMessage() = tryOrNull {
        val apiError = mapper.fromJson(string(), ApiError::class.java)
        apiError?.error
    }

    protected fun <R> ObjectResponse<R>.await(
        withProgress: Boolean = true,
        onError: OnError = ::onError,
        onSuccess: OnSuccess<R>
    ) = subscribe(withProgress, this@BasePresenterImpl, onError) { response ->
        onSuccess(response.data)
    }.also { it.connect() }

    protected fun <R> ListResponse<R>.awaitList(
        withProgress: Boolean = true,
        onError: OnError = ::onError,
        onSuccess: OnSuccess<List<R>>
    ) = subscribe(withProgress, this@BasePresenterImpl, onError) { response ->
        onSuccess(response.data)
    }.also { it.connect() }


    private fun Disposable.connect() {
        compositeDisposable.add(this)
    }

    override fun backButtonClick() {
        view?.onBackButtonClicked()
    }

}