package com.ngse.sudak.ui.navigation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.ngse.sudak.R
import com.ngse.sudak.di.PreferencesProvider
import com.ngse.sudak.ext.tryTo
import com.ngse.sudak.ui.activities.SplashActivity
import com.ngse.sudak.ui.activities.edit_profile.EditProfileActivity
import com.ngse.sudak.ui.activities.login.LoginActivity
import com.ngse.sudak.ui.activities.main.MainActivity
import com.ngse.sudak.ui.activities.profile.ProfileActivity
import com.ngse.sudak.ui.activities.numbers.NumbersActivity
import com.ngse.sudak.ui.activities.registration.RegistrationActivity
import com.ngse.sudak.ui.activities.vacancy.VacanciesActivity
import com.ngse.sudak.ui.activities.vacancy.VacanciesDetailActivity
import java.lang.ref.WeakReference
import kotlin.reflect.KClass

abstract class AbstractViewNavigator : ViewNavigator {

    protected abstract val fragmentActivity: FragmentActivity?
    protected abstract val fragment: Fragment?
    private val localStorage by PreferencesProvider()

    override fun clearBackStack() {
        val fragmentManager = fragmentActivity?.supportFragmentManager ?: return
        for (i in 0 until fragmentManager.backStackEntryCount) fragmentManager.popBackStack()
    }


    inline fun <reified T : Any> clearBackStackToFragment() {
        val fragmentManager = fragmentActivity?.supportFragmentManager ?: return
        for (i in 0 until fragmentManager.backStackEntryCount) {
            val fragment = fragmentManager.findFragmentById(R.id.container)
            if (fragment is T) {
                return
            } else
                fragmentManager.popBackStack()
        }
    }


    override fun startActivity(activityClass: KClass<out Activity>, clearBackStack: Boolean) {
        fragmentActivity.let { activity ->
            startActivity(Intent(activity, activityClass.java))
            if (clearBackStack) activity?.finishAffinity()
        }
    }

    override fun startActivity(intent: Intent) {
        fragmentActivity.let { activity ->
            activity?.startActivity(intent)
        }
    }

    override fun showCallPhone(phone: String) {
        createActivityIntent(
            fragmentActivity,
            Screens.DIALER_SCREEN,
            phone
        )?.let { startActivity(it) }
    }

    override fun showSendEmail(email: String) {
        createActivityIntent(
            fragmentActivity,
            Screens.SEND_EMAIL_SCREEN,
            email
        )?.let { startActivity(it) }
    }

    override fun showWebBrowser(url: String) {
        createActivityIntent(
            fragmentActivity,
            Screens.WEB_BROWSER_SCREEN,
            url
        )?.let { startActivity(it) }
    }

    fun createActivityIntent(context: Context?, screenKey: String?, data: Any?): Intent? =
        when (screenKey) {
            Screens.DIALER_SCREEN -> {
                Intent(Intent.ACTION_DIAL).apply {
                    setData(Uri.parse("tel:${data as String}"))
                }
            }
            Screens.SEND_EMAIL_SCREEN -> {
                val emailIntent = Intent(
                    Intent.ACTION_SENDTO,
                    Uri.fromParts("mailto", data as String, null)
                )

                Intent.createChooser(emailIntent, context!!.getString(R.string.send_email))
            }
            Screens.WEB_BROWSER_SCREEN -> {
                Intent(Intent.ACTION_VIEW).apply {
                    (data as String).let {
                        if (it.startsWith("http://") || it.startsWith("https://")) {
                            setData(Uri.parse(it))
                        } else {
                            setData(Uri.parse("http://$it"))
                        }
                    }
                }
            }
            else -> null
        }

    override fun showMainActivity() {
        startBottomMenuActivity(MainActivity::class)
    }

    override fun showMapActivity() {
        startBottomMenuActivity(ProfileActivity::class)
    }

    override fun showVacanciesActivity() {
        startBottomMenuActivity(VacanciesActivity::class)
    }

    override fun showNumbersActivity() {
        startBottomMenuActivity(NumbersActivity::class)
    }

    override fun showVacancyDetailActivity() {
        startBottomMenuActivity(VacanciesDetailActivity::class)
    }

    override fun showLoginActivity() {
        startActivity(LoginActivity::class)
    }

    override fun showSplashActivity() {
        startActivity(SplashActivity::class, true)
    }

    override fun showRegistrationActivity() {
        startActivity(RegistrationActivity::class)
    }

    override fun showEditProfile() {
        startActivity(EditProfileActivity::class)
    }

    private fun <T : Activity> startBottomMenuActivity(
        activityClass: KClass<T>,
        vararg sharedElements: Pair<View?, String?>
    ) {
        fragmentActivity?.let { activity ->
            val activityOptionsCompat =
                ActivityOptionsCompat.makeSceneTransitionAnimation(
                    activity,
                    *sharedElements
                )
            startBottomMenuActivity(activityClass, activityOptionsCompat)
        }
    }

    private fun <T : Activity> startBottomMenuActivity(
        activityClass: KClass<T>, sharedElement: View?,
        sharedElementTag: String?
    ) {
        fragmentActivity?.let { activity ->
            startBottomMenuActivity(
                activityClass, if (sharedElement != null && sharedElementTag != null)
                    ActivityOptionsCompat.makeSceneTransitionAnimation(
                        activity,
                        sharedElement,
                        sharedElementTag
                    ) else null
            )
        }
    }

    private fun <T : Activity> startBottomMenuActivity(
        activityClass: KClass<T>, activityOptionsCompat: ActivityOptionsCompat?
    ) {
        fragmentActivity?.let { activity ->
            val intent = Intent(activity, activityClass.java)
//            intent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP
            activity.startActivity(intent, activityOptionsCompat?.toBundle())
        }
    }

    private fun startBottomMenuActivity(intent: Intent) {
        fragmentActivity?.let { activity ->
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP
            activity.startActivity(intent)
        }
    }


    private fun Fragment.add(toBackStack: Boolean = true) = addOrReplace(toBackStack, true)
    private fun Fragment.replace(toBackStack: Boolean = false) = addOrReplace(toBackStack, false)

    private fun Fragment.addOrReplace(toBackStack: Boolean, add: Boolean) = tryTo {
        val fragmentManager = fragmentActivity?.supportFragmentManager ?: return
        val transaction = fragmentManager.beginTransaction()
        transaction.toBackStack(toBackStack).addOrReplace(this, add).commit()
    }

    private fun FragmentTransaction.toBackStack(toBackStack: Boolean) = if (toBackStack) {
        addToBackStack(null)
        setCustomAnimations(R.anim.from_right, R.anim.to_left, R.anim.from_left, R.anim.to_right)
    } else {
        setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
    }

    private fun FragmentTransaction.addOrReplace(fragment: Fragment, add: Boolean) =
        if (add) add(R.id.container, fragment) else replace(R.id.container, fragment)


}

class ActivityViewNavigator(fragmentActivity: FragmentActivity) : AbstractViewNavigator() {
    override val fragment: Fragment? get() = null
    override val fragmentActivity get() = reference.get()
    private val reference = WeakReference(fragmentActivity)
}

class FragmentViewNavigator(override val fragment: Fragment?) : AbstractViewNavigator() {
    override val fragmentActivity get() = reference.get()?.activity
    private val reference = WeakReference(fragment)
}
