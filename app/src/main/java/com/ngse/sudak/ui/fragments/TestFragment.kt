package com.ngse.sudak.ui.fragments

import androidx.core.os.bundleOf
import com.ngse.sudak.R
import com.ngse.sudak.ui.base.BaseFragment

class TestFragment : BaseFragment() {
    override val layoutRes: Int = R.layout.fragment_test

    companion object {
        private const val KEY_FILTERS =
            "TestFragment.KEY_FILTERS"

        fun newInstance(
            filterTemplate: String
        ): TestFragment {
            return TestFragment().apply {
                arguments = bundleOf(KEY_FILTERS to filterTemplate)

            }
        }
    }


}