package com.ngse.sudak.ui.custom

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.AnimationDrawable
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatImageView
import com.dinuscxj.refresh.IRefreshStatus
import com.ngse.sudak.R
import com.ngse.sudak.ext.dpToPixels
import com.ngse.sudak.ext.getColorCompat
import kotlin.math.min

class RefreshCircleLoader(context: Context) : AppCompatImageView(context), IRefreshStatus {

    private var isRefreshing: Boolean = false
    private var needToUpdateView: Boolean = false
    private var rotationDegrees: Float = 0f

    private val rotationRunnable: Runnable

    companion object {
        private const val MAX_DEGREES = 360
        private const val ONE_ROTATION_DEGREES = 30
        private const val ROTATION_FREQUENCY = 80L
        private const val PULL_ROTATION_ANIM_FACTOR = 0.9f
        private const val SIZE = 40f
    }

    val layoutParams
        get() = LinearLayout.LayoutParams(
            context.dpToPixels(SIZE).toInt(),
            context.dpToPixels(SIZE).toInt()
        ).apply {
            context.dpToPixels(4f).toInt().let {
                setPadding(0, it, 0, it)
            }
        }

    init {
        setImageResource(R.drawable.ic_circle_loader)
        setColorFilter(context.getColorCompat(R.color.colorPrimary))
        rotationRunnable = object : Runnable {
            override fun run() {
                rotationDegrees += ONE_ROTATION_DEGREES
                if (rotationDegrees >= MAX_DEGREES) {
                    rotationDegrees -= MAX_DEGREES
                }

                invalidate()
                if (needToUpdateView && isRefreshing) {
                    postDelayed(this, ROTATION_FREQUENCY)
                }
            }
        }
    }

    override fun onDraw(canvas: Canvas) {
        canvas.rotate(rotationDegrees, width / 2f, height / 2f)
        super.onDraw(canvas)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        needToUpdateView = true
    }

    override fun onDetachedFromWindow() {
        needToUpdateView = false
        super.onDetachedFromWindow()
    }

    override fun refreshing() {
        isRefreshing = true
        post(rotationRunnable)
    }

    override fun reset() {
        isRefreshing = false
        rotationDegrees = 0f
    }

    override fun pullProgress(pullDistance: Float, pullProgress: Float) {
        rotationDegrees = MAX_DEGREES * min(1.0f, pullProgress * PULL_ROTATION_ANIM_FACTOR)
        invalidate()
    }

    override fun pullToRefresh() {}
    override fun refreshComplete() {}

    override fun releaseToRefresh() {}
}