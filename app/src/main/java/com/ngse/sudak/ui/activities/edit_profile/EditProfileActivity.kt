package com.ngse.sudak.ui.activities.edit_profile

import android.Manifest
import android.os.Bundle
import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import com.ngse.sudak.R
import com.ngse.sudak.data.UserProfile
import com.ngse.sudak.ext.ui.initChooserDialog
import com.ngse.sudak.ext.ui.load
import com.ngse.sudak.ext.withPermissions
import com.ngse.sudak.ui.base.BaseActivity
import gun0912.tedbottompicker.TedBottomPicker
import kotlinx.android.synthetic.main.activity_edit_profile.*

class EditProfileActivity : BaseActivity() {

    override val layoutRes: Int = R.layout.activity_edit_profile

    private var city: String? = null
    private var photoPath: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ivProfile.setOnClickListener {
            withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) {
                ivProfile.pick()
            }
        }
        etCity.setOnClickListener {
            initChooserDialog(listOf("Донецк", "Зугрес", "Павловск")) {
                etCity.setText(it)
                city = it
            }
        }
        btnEdit.setOnClickListener {
            preferences.userProfile = generateParams()
        }
    }

    private fun generateParams(): UserProfile {
        val surname = etFio.text.toString()
        val name = etName.text.toString()
        val middlename = etMiddleName.text.toString()
        val interests = etInterests.text.toString()
        val age = etAge.text.toString().toIntOrNull() ?: 0
        val number = etPhoneNumber.text.toString()
        return UserProfile(surname, name, middlename, interests, city, age, number, photoPath)
    }


    private fun ImageView.pick() = TedBottomPicker.with(this@EditProfileActivity as FragmentActivity).show {
        photoPath = it.path
        load(it)
    }
}
