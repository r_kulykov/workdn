package com.ngse.sudak.ui.activities.registration

import android.os.Bundle
import android.os.Handler
import com.ngse.sudak.R
import com.ngse.sudak.ext.ui.toast
import com.ngse.sudak.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_registration.*

class RegistrationActivity : BaseActivity() {

    override val layoutRes: Int = R.layout.activity_registration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginButton.setOnClickListener {
            showProgressIndicator()
            Handler().postDelayed({
                toast(R.string.registration_successful)
                navigator.showSplashActivity()
            }, 3500L)
        }
    }

}