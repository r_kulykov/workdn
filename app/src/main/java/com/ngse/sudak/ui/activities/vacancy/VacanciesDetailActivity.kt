package com.ngse.sudak.ui.activities.vacancy

import android.os.Bundle
import android.os.Handler
import com.ngse.sudak.R
import com.ngse.sudak.data.Vacancy
import com.ngse.sudak.ext.ui.toast
import com.ngse.sudak.ui.adapters.VacancyAdapter
import com.ngse.sudak.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_vacancy_detail.*

class VacanciesDetailActivity : BaseActivity() {

    override val layoutRes: Int = R.layout.activity_vacancy_detail

    private val adapter = VacancyAdapter()

    private val vacancies = listOf<Vacancy>(
        Vacancy(
            "Web Design",
            "15000$",
            "Ngse",
            "New York",
            "last updated 20.02.2021"
        ), Vacancy(
            "Механик",
            "1000$",
            "ООО `METINVEST`",
            "Корыстень",
            "last updated 20.02.2021"
        ), Vacancy(
            "Водитель",
            "10$",
            "Похорон бюро",
            "Россошь",
            "last updated 20.02.2020"
        ), Vacancy(
            "Клоун",
            "150$",
            "Полиция",
            "Донецк",
            "last updated 10.04.2021"
        )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rvVacancies.adapter = adapter
        tvStatus.setOnClickListener { onBackPressed() }
        tvPhoneFirst.setOnClickListener { navigator.showCallPhone("+38(099)-678-45-67") }
        tvPhoneSecond.setOnClickListener { navigator.showCallPhone("+380951062944") }
        btnSendCV.setOnClickListener {
            showProgressIndicator()
            Handler().postDelayed({
                btnSendCV.run {
                    setText(R.string.sent)
                    isEnabled = false
                }
                toast(R.string.already_sent)
                hideProgressIndicator()
            }, 1500L)
        }
        tvRules.text = "- Опыт работы приветствуется, грамотное письмо;\n" +
                "\n" +
                "- Свободный серфинг в социальных сетях (Инстаграм) и Интернет;\n" +
                "\n" +
                "- Инициативность и креативность — без них никак!\n"
        tvDescription.text = "Вы амбициозная, креативная, умеете добиваться поставленной цели? Тогда у нас для Вас есть вакансия!\n" +
                "\n" +
                "В крупную стабильную компанию требуется Помощник руководителя."
        adapter.setItems(vacancies)
    }


}
