package com.ngse.sudak.ui.activities.login

import com.ngse.sudak.ui.base.BasePresenter
import com.ngse.sudak.ui.base.BasePresenterImpl
import moxy.InjectViewState

@InjectViewState
class LoginPresenter : BasePresenterImpl<LoginView>(){

    override val view: LoginView = viewState



}