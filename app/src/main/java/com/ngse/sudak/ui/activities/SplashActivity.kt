package com.ngse.sudak.ui.activities

import android.os.Bundle
import com.ngse.sudak.R
import com.ngse.sudak.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : BaseActivity() {

    override val layoutRes: Int = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        btnSearchVacancies.setOnClickListener { navigator.showVacanciesActivity() }
        btnSearchWorkers.setOnClickListener { navigator.showVacanciesActivity() }
    }
}
