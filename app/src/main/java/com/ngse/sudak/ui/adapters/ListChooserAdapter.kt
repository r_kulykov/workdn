package com.ngse.sudak.ui.adapters

import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.ngse.sudak.R
import com.ngse.sudak.ui.base.BaseAdapter
import com.ngse.sudak.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.list_item_spinner.view.*


class ListChooserAdapter(
    private val onItemClicked: (String) -> Unit
) :
    BaseAdapter<String, BaseViewHolder<String>>() {

    override fun newViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<String> =
        ViewHolder(R.layout.list_item_spinner, parent)


    inner class ViewHolder(@LayoutRes layoutRes: Int, parent: ViewGroup) :
        BaseViewHolder<String>(layoutRes, parent) {

        override fun updateView(item: String) = with(itemView) {
            tvTitle.text = item
            setOnClickListener { onItemClicked(item)}
        }
    }


}


