package com.ngse.sudak.ui.base

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import com.ngse.sudak.R
import com.ngse.sudak.ext.ui.hideKeyboard
import com.ngse.sudak.ext.ui.toast
import com.ngse.sudak.ui.navigation.FragmentViewNavigator


abstract class BaseFragment : Fragment(),
    BaseView {


    protected lateinit var layoutProgressBar: FrameLayout

    abstract val layoutRes: Int
    protected lateinit var progressBar: ProgressBar

    override fun closeKeyboard() = hideKeyboard()

    override fun showMessage(text: String?) = toast(text)
    override fun showMessage(textId: Int) = toast(textId)
    val navigator by lazy { FragmentViewNavigator(this) }


    override fun onCreateView(inflater: LayoutInflater, parent: ViewGroup?, state: Bundle?): View? {
        return inflater.createContentView()
    }

    private fun LayoutInflater.createContentView() = FrameLayout(context).apply {
        layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        if (layoutRes != 0) inflate(layoutRes, this)

        layoutProgressBar = FrameLayout(requireContext())
        layoutProgressBar.isClickable = true
        layoutProgressBar.visibility = View.GONE
        addView(layoutProgressBar, layoutParams)
        progressBar = layoutProgressBar.addProgressBar()
    }



    private fun FrameLayout.addProgressBar(): ProgressBar {
        return ProgressBar(context).apply {
            visibility = View.INVISIBLE
            val size = resources.getDimension(R.dimen.progress_bar_size).toInt()
            val params = FrameLayout.LayoutParams(size, size).apply { gravity = Gravity.CENTER }
            addView(this, params)

        }
    }

    override fun onDestroyView() {
        activity?.hideKeyboard()
        super.onDestroyView()
    }

    override fun showProgressIndicator() = run {
        layoutProgressBar.visibility = View.VISIBLE
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgressIndicator() = run {
        progressBar.visibility = View.GONE
        layoutProgressBar.visibility = View.GONE
    }

    override fun runOnUiThread(action: Runnable) {
        activity?.runOnUiThread(action)
    }


    override fun onBackButtonClicked() {
        activity?.onBackPressed()
    }
}
