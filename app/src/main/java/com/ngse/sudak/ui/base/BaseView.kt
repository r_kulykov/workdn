package com.ngse.sudak.ui.base

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface BaseView : MvpView{


    fun runOnUiThread(action: Runnable)

    fun closeKeyboard()

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(text: String?)
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(textId: Int)
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun onBackButtonClicked()

    fun showProgressIndicator()
    fun hideProgressIndicator()
}

