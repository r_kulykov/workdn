package com.ngse.sudak.ui.activities.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ngse.sudak.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
