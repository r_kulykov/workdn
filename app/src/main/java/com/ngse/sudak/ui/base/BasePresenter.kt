package com.ngse.sudak.ui.base

import com.ngse.sudak.ui.navigation.ViewNavigator


interface BasePresenter<V : BaseView> {
    val view: V?
    fun setNavigator(navigator: ViewNavigator)
    fun getNavigator():ViewNavigator
    fun backButtonClick()
}