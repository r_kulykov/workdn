package com.ngse.sudak.ui.activities.profile

import android.os.Bundle
import com.ngse.sudak.R
import com.ngse.sudak.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : BaseActivity() {

    override val layoutRes: Int = R.layout.activity_profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        btnEdit.setOnClickListener { navigator.showEditProfile() }
    }
}
