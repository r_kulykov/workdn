package com.ngse.sudak.ui.base

import android.app.Dialog
import android.os.Bundle
import android.transition.ChangeBounds
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.ngse.sudak.R
import com.ngse.sudak.di.PreferencesProvider
import com.ngse.sudak.ext.getColorCompat
import com.ngse.sudak.ext.ui.dp
import com.ngse.sudak.ui.navigation.ActivityViewNavigator
import com.wang.avi.AVLoadingIndicatorView
import kotlin.random.Random


abstract class BaseActivity : AppCompatActivity() {


    companion object {
        const val FRAGMENT_KEY = "FragmentWrapperActivity.FRAGMENT_ADDED"
    }

    abstract val layoutRes: Int
    protected val localStorage by PreferencesProvider()
    protected lateinit var dialog: Dialog
    val navigator by lazy { ActivityViewNavigator(this) }
    private var menu: Menu? = null
    val preferences by PreferencesProvider()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ChangeBounds().apply {
            duration = 500 //SceneTransitionAnimation duration
            window.sharedElementEnterTransition = this
        }
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        setContentView(LayoutInflater.from(this@BaseActivity).createContentView())

    }

    override fun onResume() {
        super.onResume()
        setRndLogo()
        findViewById<ImageView>(R.id.logoImageView)?.setOnClickListener { setRndLogo() }
    }

    fun setRndLogo() {
        val rndVal = Random.nextInt(0, 4)
        val resId = when (rndVal) {
            0 -> R.drawable.ic_launcher_rect_rounded_red
            1 -> R.drawable.ic_launcher_circle_white_and_red_stroke
            2 -> R.drawable.ic_launcher_circle_red_and_white_stroke
            3 -> R.drawable.ic_launcher_rect_rounded_white
            else -> R.drawable.ic_launcher_circle_red_and_white_stroke_tintable
        }
        findViewById<ImageView>(R.id.logoImageView)?.setImageResource(resId)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }


    private fun LayoutInflater.createContentView() = FrameLayout(context).apply {
        layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        if (layoutRes != 0) inflate(layoutRes, this)
        dialog = addLoadingIndicator()
    }

    private fun FrameLayout.addLoadingIndicator() = Dialog(context, R.style.AppModalDialog).apply {
        setContentView(LinearLayout(context).apply {
            orientation = LinearLayout.VERTICAL
            gravity = Gravity.CENTER
            AVLoadingIndicatorView(context).apply {
                val size = resources.getDimension(R.dimen.progress_bar_size).toInt()
                setIndicator("BallSpinFadeLoaderIndicator")
                setIndicatorColor(getColorCompat(R.color.white))
                val params = LinearLayout.LayoutParams(size, size)
                addView(this, params)
            }
            TextView(context).apply {
                setTextColor(getColorCompat(R.color.white))
                setText(R.string.please_wait)
                val params = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                ).apply {
                    setMargins(0, 5.dp, 0, 0)
                    gravity = Gravity.CENTER
                }
                addView(this, params)
            }

        })
        setCancelable(false)
    }


    fun showProgressIndicator() = run {
        dialog.show()
    }

    fun hideProgressIndicator() = run {
        dialog.hide()
    }

    private fun FrameLayout.addProgressBar() = ProgressBar(context).apply {
        visibility = View.INVISIBLE
        val size = resources.getDimension(R.dimen.progress_bar_size).toInt()
        val params = FrameLayout.LayoutParams(size, size).apply { gravity = Gravity.CENTER }
        addView(this, params)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this.menu = menu
        return super.onCreateOptionsMenu(menu)
    }

}