package com.ngse.sudak.ui.activities.vacancy

import android.os.Bundle
import android.os.Handler
import android.view.inputmethod.EditorInfo
import androidx.core.widget.doAfterTextChanged
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.dinuscxj.refresh.RecyclerRefreshLayout
import com.ngse.sudak.R
import com.ngse.sudak.data.Vacancy
import com.ngse.sudak.ext.dpToPixels
import com.ngse.sudak.ext.ui.hideKeyboard
import com.ngse.sudak.ui.adapters.VacancyAdapter
import com.ngse.sudak.ui.base.BaseActivity
import com.ngse.sudak.ui.custom.RefreshCircleLoader
import kotlinx.android.synthetic.main.activity_vacancies.*

class VacanciesActivity : BaseActivity()
    , SwipeRefreshLayout.OnRefreshListener
    , RecyclerRefreshLayout.OnRefreshListener {

    override val layoutRes: Int = R.layout.activity_vacancies

    private val adapter = VacancyAdapter()

    private val vacancies = listOf<Vacancy>(
        Vacancy(
            "Web Design",
            "15000$",
            "Ngse",
            "New York",
            "last updated 20.02.2021"
        ), Vacancy(
            "Механик",
            "1000$",
            "ООО `METINVEST`",
            "Корыстень",
            "last updated 20.02.2021"
        ), Vacancy(
            "Водитель",
            "10$",
            "Похорон бюро",
            "Россошь",
            "last updated 20.02.2020"
        ), Vacancy(
            "Клоун",
            "150$",
            "Полиция",
            "Донецк",
            "last updated 10.04.2021"
        )
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        swipeRefreshLayout.setOnRefreshListener(this)
        rvVacancies.adapter = adapter
        RefreshCircleLoader(this).also {
            swipeRefreshLayout.setRefreshView(it, it.layoutParams)
        }
        adapter.setItems(vacancies)
        adapter.onItemClick = {
            navigator.showVacancyDetailActivity()
        }
        etSearch.run {
            doAfterTextChanged { text -> search(text.toString()) }
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search(text.toString())
                    hideKeyboard()
                    true
                } else {
                    false
                }
            }
        }
    }

    private fun search(query: String) {
        val filtered = vacancies.filter { it.title.contains(query, true) }
        adapter.setItems(filtered)
    }

    override fun onRefresh() {
        Handler().postDelayed({
            search(etSearch.text.toString())
            swipeRefreshLayout?.let {
                it.post { it.setRefreshing(false) }
            }
        }, 2000L)

    }


}
