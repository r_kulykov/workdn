package com.ngse.sudak.ui.navigation

object Screens {
    const val DIALER_SCREEN = "dialer screen"
    const val SEND_EMAIL_SCREEN = "send email screen"
    const val WEB_BROWSER_SCREEN = "web browser screen"
}