package com.ngse.sudak

import androidx.multidex.MultiDexApplication
import com.ngse.sudak.data.LocalStorage
import com.ngse.sudak.di.ContextProvider
import com.ngse.sudak.di.PreferencesProvider

class App : MultiDexApplication() {

    init {
        ContextProvider.inject { applicationContext }
        PreferencesProvider.inject { LocalStorage(applicationContext) }
    }

    override fun onCreate() {
        super.onCreate()
    }

}
