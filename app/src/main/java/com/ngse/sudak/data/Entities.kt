package com.ngse.sudak.data

data class Vacancy(
    val title: String,
    val price: String,
    val companyName: String,
    val location: String,
    val lastUpdated: String
) {
    val isNew: Boolean = false
}

data class UserProfile(
    var surname: String,
    var name: String,
    var middlename: String,
    var interests: String,
    var city: String?=null,
    var age: Int,
    var phoneNumber: String,
    var photoPath: String? = null
)