package com.ngse.sudak.data

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class BaseData(
    @SerializedName("message") val message: String? = null
) : Serializable

data class BaseObjectResponse<T>(
    @SerializedName("data") val data: T
) : BaseData(), Serializable

data class BaseListResponse<T>(
    @SerializedName("data") val data: List<T>
) : BaseData(), Serializable

data class ApiError(
    @SerializedName("message") val error: String
)