package com.ngse.sudak.data

import android.content.Context
import android.content.Context.MODE_PRIVATE
import com.ngse.sudak.ext.data.delegate


class LocalStorage(context: Context) : Preferences {

    private val sharedPrefs by lazy { context.getSharedPreferences("sharedprefs", MODE_PRIVATE) }

    override var userProfile: UserProfile? by sharedPrefs.delegate("userProfile")


}
